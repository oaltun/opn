opn
------

opn is a framework for experimenting on metaheuristic optimization algorithms.

See the full documentation in http://oaltun.bitbucket.org/opn/index.html
